;;; init.el --- Gokul Das's Emacs configuration
;;; Commentary: Root configuration file
;; ---------------------------------------------------------------------

;; init.d is the folder containing init submodules
(defconst init.d "~/.emacs.d/init.d")

;; Function for loading init sub modules
(defun load-config-module (filename)
  "Load configuration file from init directory"
  (load (concat init.d "/" filename)))

;; Bootstrap straight and use-package
(load-config-module "boot.el")

;; UI configurations
(load-config-module "icons.el")
(load-config-module "modeline.el")
(load-config-module "gui.el")
(load-config-module "theme.el")
(load-config-module "editor.el")
(load-config-module "layout.el")

;; Editor enhancements
(load-config-module "evil.el")       ; Vim keybindings
(load-config-module "whichkey.el")   ; Show command suggestions
(load-config-module "ivy.el")        ; Completion frameworks
(load-config-module "company.el")    ; Code completion framework
(load-config-module "flycheck.el")   ; On the fly syntax checking
(load-config-module "lsp.el")        ; Language server integration
(load-config-module "spell.el")      ; Flyspell settings
(load-config-module "projectile.el") ; Projectile project management

;; Language modes
(load-config-module "markdown.el")   ; Markdown mode options
(load-config-module "rust.el")       ; Rust language support

;; Work plugings
(load-config-module "mu4e.el")       ; Mail client
(load-config-module "magit.el")      ; Git client
(load-config-module "pdf.el")        ; PDF support
(load-config-module "impatient.el")  ; HTML and MD browser live preview
(load-config-module "vterm.el")      ; Vterm with zsh
(load-config-module "org.el")        ; Org mode customization (evil)
(load-config-module "rest.el")       ; ReST client

;; Custom key bindings
(load-config-module "custombinds.el")
