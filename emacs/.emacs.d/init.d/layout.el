;; Layout management
;;
;; eyebrowse
;; ---------------------------------------------------------------------

(use-package eyebrowse
  :demand
  :init
  (setq eyebrowse-keymap-prefix (kbd "C-`")) ; Set C-` as prefix
  :config
  (eyebrowse-mode t))                        ; Start eyebrowse by default
