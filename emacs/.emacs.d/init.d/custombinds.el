;; Custom keybindings
;; ---------------------------------------------------------------------

;; Use S-<arrow> to move around splits using windmove
;; This takes an optional argument for modifier if needed
(windmove-default-keybindings)

;; Org global keybinds
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
