;; GUI configurations
;; ---------------------------------------------------------------------

;; Hide toolbar
(tool-bar-mode -1)

;; Hide scrollbar
(scroll-bar-mode -1)

;; Hide menubar
(menu-bar-mode -1)

;; Set default font
(custom-set-faces
 '(default ((t (
                :family "FiraCode Nerd Font"
                :foundry "CTDB"
                :slant normal
                :weight normal
                :height 110
                :width normal)))))
