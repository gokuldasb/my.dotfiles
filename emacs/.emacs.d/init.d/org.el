;; Org mode customization
;;
;; ---------------------------------------------------------------------

;; Hook callback for key themes to enable
(defun set-evil-org-key-themes ()
  (evil-org-set-key-theme
   '(textobjects insert return navigation additional shift todo heading)))

;; Enable evil mode specialization
(use-package evil-org
  :after (evil org)
  :hook
  (org-mode . evil-org-mode)                ; Launch evil-org-mode along with org-mode
  (evil-org-mode . set-evil-org-key-themes)
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))
