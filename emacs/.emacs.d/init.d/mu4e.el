;; Company mode configurations
;;
;; Mail client with mu indexer
;; ---------------------------------------------------------------------

(use-package mu4e
  :init
  (setq mail-user-agent 'mu4e-user-agent)                       ; use mu4e for e-mail in emacs
  :commands mu4e
  :custom
  (mu4e-maildir "/home/user/.mail/personal/")
  (mu4e-update-interval 180)                                    ; Update every 3 minutes
  (mu4e-html2text-command "/usr/bin/w3m -T text/html")
  (mu4e-sent-folder   "/Sent")
  (mu4e-drafts-folder "/Drafts")
  (mu4e-trash-folder  "/Trash")
  (mu4e-refile-folder "/Archive")
  (message-send-mail-function 'message-send-mail-with-sendmail)
  (sendmail-program "/usr/bin/msmtp")
  (message-sendmail-extra-arguments '("--read-envelope-from"))  ; Choose outgoing SMTP server based on from-field
  (message-sendmail-f-is-evil 't)                               ; don't keep message buffers around
  (message-kill-buffer-on-exit t))

(use-package mu4e-alert
  :demand t
  :hook ((after-init . mu4e-alert-enable-mode-line-display)     ; Enable mail count in modeline
         (after-init . mu4e-alert-enable-notifications))        ; Enable notifications
  :config (mu4e-alert-set-default-style 'libnotify))            ; Use libnotify for notifications
