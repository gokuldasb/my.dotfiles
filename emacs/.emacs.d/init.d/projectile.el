;; Projectile for project management
;; ---------------------------------------------------------------------

(use-package projectile
  :config
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1)
  :custom
  (projectile-completion-system 'ivy)
  ;; Project search paths
  ;; Use M-x projectile-discover-projects-in-search-path
  (projectile-auto-discover nil)
  (projectile-project-search-path '("~/Projects" "~/Work")))
