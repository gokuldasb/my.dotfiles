;; Markdown mode options
;; ---------------------------------------------------------------------

;; Hook for markdown mode
(defun initialize-markdown ()
  (setq fill-column 100)                 ; Set line width to 100
  (auto-fill-mode t)                     ; Enable wrapping
  (display-fill-column-indicator-mode t) ; Enable line limit rule
  (flyspell-mode t))                     ; Enable spell checker

(use-package markdown-mode
  :hook (markdown-mode . initialize-markdown)
  :custom
  (evil-shift-width 2)                             ; Evil shifts should be 2 spaces
  (tab-width 2)                                    ; Indents should be 2 spaces
  (markdown-indent-on-enter 'indent-and-new-item)) ; Create new list item on enter
