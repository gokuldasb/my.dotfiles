;; VTerm terminal
;; ---------------------------------------------------------------------

(use-package vterm
  :commands vterm                       ; Wait till vterm command is given
  :bind ("C-<return>" . vterm)          ; or till C-return
  :custom (vterm-shell "/usr/bin/zsh")) ; Set Zsh as default shell
