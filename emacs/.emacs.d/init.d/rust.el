;; Rust Language Mode
;;
;; This uses rustic fork of rust-mode
;; ---------------------------------------------------------------------

(use-package rustic
  :after (lsp-mode))
