;; Company mode configurations
;;
;; Code completion framework
;; ---------------------------------------------------------------------

(use-package company
  :delight
  :hook (after-init . global-company-mode)) ; enable in all buffers
