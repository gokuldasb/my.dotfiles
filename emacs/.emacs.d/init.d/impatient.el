;; Impatient mode for live html and markdown preview
;;
;; This will install simple-httpd as a dependency
;; ---------------------------------------------------------------------

(use-package impatient-mode
  :commands impatient-mode)

;; Create filter for markdown. It uses JS from CDN to render MD
;; https://stackoverflow.com/a/36189456
(defun markdown-html (buffer)
  (princ (with-current-buffer buffer
	   (format
	    "<!DOCTYPE html>
             <html>
               <title>Impatient Markdown</title>
               <xmp theme=\"united\" style=\"display:none;\"> %s  </xmp>
               <script src=\"http://strapdownjs.com/v/0.2/strapdown.js\"></script>
             </html>"
	    (buffer-substring-no-properties (point-min) (point-max))))
	 (current-buffer)))

;; Command to launch impatient markdown
(defun impatient-markdown ()
  (interactive)
  (httpd-start)
  (impatient-mode t)
  (imp-set-user-filter 'markdown-html)
  (browse-url-xdg-open "http://localhost:8080/imp"))
