;; Spell checker configuration
;;
;; Uses flyspell with hunspell
;; ---------------------------------------------------------------------

;; Set hunspell as spell checker and Canadian English for dictionary
(setq ispell-program-name (executable-find "hunspell")
      ispell-dictionary "en_CA")

;; Enable spelling suggestions through ivy
(use-package flyspell-correct
  :after (flyspell ivy)
  :bind (:map flyspell-mode-map ("C-$" . flyspell-correct-wrapper)))

(use-package flyspell-correct-ivy
  :after flyspell-correct)
