;; Ivy, Swiper and Counsel
;;
;; Ivy: Completion mechanism
;; Counsel: Ivy customization for specific tasks
;; Swiper: I-search alternative
;; ---------------------------------------------------------------------

(use-package counsel
  :delight
  (ivy-mode)
  (counsel-mode)
  :config
  (ivy-mode 1)      ; start ivy-mode by default
  (counsel-mode 1)) ; start counsel-mode by default
