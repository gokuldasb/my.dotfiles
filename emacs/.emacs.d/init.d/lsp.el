;; Language Server Plugin
;;
;; Uses LSP-Mode
;; ---------------------------------------------------------------------

(use-package lsp-mode
  :hook
  ((python-mode . lsp)
   (c++-mode . lsp)
   (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp
  :custom
  (lsp-keymap-prefix "C-c C-l"))                  ; Key map prefix

(use-package lsp-ui
  :commands lsp-ui-mode
  :after (lsp-mode))

(use-package lsp-ivy
  :commands lsp-ivy-workspace-symbol
  :after (lsp-mode counsel))

(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list
  :after (lsp-mode))
