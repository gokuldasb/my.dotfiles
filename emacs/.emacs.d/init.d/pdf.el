;; PDF Tools
;; ---------------------------------------------------------------------

(use-package pdf-tools
  :mode ("\\.pdf\\'" . pdf-view-mode)                          ; Defer till PDF is opened
  :hook
  (pdf-view-mode . (lambda() (display-line-numbers-mode -1)))  ; Turn off line number
  :config
  (pdf-loader-install)                                         ; Speed up emacs starting
  (setq TeX-view-program-selection '((output-pdf "pdf-tools")))
  (setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view"))))
