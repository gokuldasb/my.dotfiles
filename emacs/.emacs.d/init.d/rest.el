;; ReST client
;; ---------------------------------------------------------------------

(use-package restclient
  :commands restclient-mode
  :mode (("\\.http\\'" . restclient-mode)))
