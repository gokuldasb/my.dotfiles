;; Evil mode configurations
;;
;; Evil makes it possible to use vim keybindings in emacs
;; ---------------------------------------------------------------------

(use-package evil
  :demand t
  :custom
  (evil-want-keybinding nil)         ; Solution for some weird bug
  (evil-default-state 'normal)       ; Start in normal mode by default
  :config
  (evil-mode 1))                     ; start evil mode by default


;; Evil collections for modes not covered by default
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))
