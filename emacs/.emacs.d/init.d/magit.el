;; Magit git plugin
;;
;; ---------------------------------------------------------------------

(use-package magit
  :defer t
  :commands (magit magit-status)
  :bind ("C-x g" . magit-status))
