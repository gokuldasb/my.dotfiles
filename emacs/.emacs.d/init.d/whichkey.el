;; Which Key
;;
;; Which key shows suggestions for incomplete commands in mini-buffer
;; ---------------------------------------------------------------------

(use-package which-key
  :demand t
  :delight
  :config
  (which-key-mode))
