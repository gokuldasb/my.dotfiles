;; Modeline management
;; ---------------------------------------------------------------------

;; Use delight to reduce modeline length
(use-package delight
  :demand )


;; Use Telephone line for modeline
(use-package telephone-line
  :config
  (telephone-line-mode 1)
  :custom
  ;; Separattor theming
  (telephone-line-primary-left-separator 'telephone-line-tan-left)
  (telephone-line-secondary-left-separator 'telephone-line-tan-hollow-left)
  (telephone-line-primary-right-separator 'telephone-line-tan-right)
  (telephone-line-secondary-right-separator 'telephone-line-tan-hollow-right)
  ;; Short evil state
  (telephone-line-evil-use-short-tag t))
