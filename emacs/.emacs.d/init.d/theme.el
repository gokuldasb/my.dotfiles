;; Theme settings
;; ---------------------------------------------------------------------

;; Monokai theme
(use-package monokai-theme
  :demand t
  :config
  (load-theme 'monokai t))


;; Alect theme
(use-package alect-themes
  :disabled
  :config
  (load-theme 'alect-dark t))


;; TangoTango theme
(use-package tangotango-theme
  :disabled
  :config
  (load-theme 'tangotango t))
