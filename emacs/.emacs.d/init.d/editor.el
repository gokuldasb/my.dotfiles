;; Editor configurations
;; ---------------------------------------------------------------------

;; Keep autosave and backup files in special directory
(setq backupdir "~/.saves/")
(setq backup-directory-alist `((".*" . ,backupdir)))         ; Move backups
(setq auto-save-file-name-transforms `((".*" ,backupdir t))) ; Move Auto saves
(setq backup-by-copying t)                                   ; Backup by copying

;; Highlight matching paranthesis
(setq show-paren-delay 0)            ; Don't wait to show
(show-paren-mode t)                  ; turn paren-mode on
(setq show-paren-style 'paranthesis) ; alternatives are 'mixed' and 'expression'

;; Enable line and column numbers
(line-number-mode 1)                 ; Line number in mode line
(column-number-mode 1)               ; Column number in mode line
(global-display-line-numbers-mode 1) ; Line numbers on left margin

;; Disable tabbed indent by default
(setq-default indent-tabs-mode nil)  ; Disable tabs
