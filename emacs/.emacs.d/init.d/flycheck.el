;; Flycheck configurations
;;
;; On-the-fly syntax checking
;; ---------------------------------------------------------------------

(use-package flycheck
  :delight
  :config
  (global-flycheck-mode)) ; Enable flycheck by default in all buffers
